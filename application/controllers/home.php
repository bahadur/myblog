<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){

		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('ci_security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
	}

	public function index()
	{
		$data['content'] = 'contents/home';
		$this->load->view('template/index',$data);
	}

	public function add_post(){

		if (!$this->tank_auth->is_logged_in()) {									// logged in
			redirect('auth/login');
		}

		$data['content'] = 'contents/add_post';
		$this->load->view('template/index',$data);	
	}
}
