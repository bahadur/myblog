<div style="margin-top: 80px; border: 1px solid red; padding: 10px">
		<?php echo form_open($this->uri->uri_string(), array('class' => 'form-horizontal')); ?>
		<div class="form-group">
			<?php echo form_label($login_label, $login['id'], array('class' => 'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo form_input($login); ?>
				<div class="text-danger">
					<?php echo form_error($login['name']); ?>
					<?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
				</div>
			</div>
		</div>


		<div class="form-group">
			<?php echo form_label('Password', $password['id'], array('class' => 'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo form_password($password); ?>
				<div class="text-danger">
					<?php echo form_error($password['name']); ?>
					<?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
				</div>
			</div>
		</div>


		<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <div class="checkbox">
		        
			        <?php echo form_checkbox($remember); ?> 
			        <?php echo form_label('Remember me', $remember['id']); ?>
		        
		      </div>
		    </div>
		</div>


		<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <?php echo form_submit('submit', 'Let me in', array('class'=>'btn btn-default')); ?>
		      <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>
		    </div>
		</div>

		<?php echo form_close(); ?>

</div>