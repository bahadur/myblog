<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My Blogs</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-wysiwyg.css">
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/bootstrap-wysiwyg.js"></script>

</head>
<body>

  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">My Blog</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?php echo base_url() ?>">Home</a></li>
          <li><a href="<?php echo base_url()?>home/add_post">New Post</a></li>
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
          <?php if (!$this->tank_auth->is_logged_in()) { ?>
          <li><a href="<?php echo base_url()?>auth/login">Login</a></li>
          <?php } else {?>
          <li><a href="<?php echo base_url()?>auth/logout">Logout</a></li>
          <?php } ?>
        </ul>
      </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
  </nav>
    

  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <?php $this->load->view($content) ?>
      </div>
      <div class="col-md-4">
        
      </div>
    </div> 
  </div>
</body>
</html>